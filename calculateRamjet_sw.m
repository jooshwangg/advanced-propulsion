function [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust...
            eta_th,eta_prop,T_1, M_burnerIn] = calculateRamjet(p_inf,T_inf,M_flight,...
            S_shock,T_burnerIn,T_burn,T_req) %swapped M_burn and added it as an input
%% Constants
gamma = 1.4;
R = 287;

%% Variables
%Test input values: p_inf=70e3; T_inf=210; M_flight=2.8; S_shock=1.1;
%M_burn=0.2; T_burn=1700; T_req=1000
P_1 = p_inf;
T_1 = T_inf;
M_inlet = M_flight;
M_shock = S_shock;
%T_burnerIn = T_burnerIn;
T_b = T_burn;
Thrust = T_req;

%% Flow properties at different stages in burner
% See Equations.m for isentropic flow and normal shock relations used to
% calculate flow properties

%% Inlet
%Temperature ratio To1/T1 & Throat Temperature Ratio T01/T*
T01_1 = Equations.tempRatio_Isen(M_inlet);
T01_C1 = Equations.tempRatio_Isen(1);

% %Pressure Ratio P01/P & Throat Pressure Ratio P01/PC1
P01_P1 = Equations.pressRatio_Isen(M_inlet);
P01_C1 = Equations.pressRatio_Isen(1);

%Inlet Area Ratio (Isentropic Equation) [Ratio of Inlet to Throat]
A1_C1 = Equations.areaRatio_Isen(M_inlet);

%% Throat
%Throat Pressure
P_C1=P_1*P01_P1/P01_C1;
%Throat Temperature
T_C1=T_1*T01_1/T01_C1;

%% Throat to Shock
% where x is point immediately before shock and y is point immediately
% after shock
%density ratio across normal shock of strength M_shock
rhoy_rhox = Equations.rhoRatio_NShock(M_shock);
%pressure ratio
Py_Px = Equations.pressRatio_NShock(M_shock);
%area ratio between location of shock and inlet throat
Ashock_AC1 = Equations.areaRatio_Isen(M_shock);
%Pressure Ratio
P0x_Px = Equations.pressRatio_Isen(M_shock);
%temperature ratio 
T0x_Tx = Equations.tempRatio_Isen(M_shock);

%% Immediately after shock
%Mach number after shock
My = Equations.machNum_NShock(M_shock);
%Ay* is area required to choke flow after shock
Ashock_Aystar = Equations.areaRatio_Isen(My); 
%Temperature Ratio
T0y_Ty = Equations.tempRatio_Isen(My);
%Pressure Ratio
P0y_Py=Equations.pressRatio_Isen(My);
%% Burner Entry
M_burnerIn = sqrt(((T_1*T0y_Ty*(Py_Px/rhoy_rhox)*(1/T0x_Tx)*T01_1)/(T_burnerIn)-1)/((gamma-1)/2));
A2_Aystar = Equations.areaRatio_Isen(M_burnerIn);
%T0y_T2 = Equations.tempRatio_Isen(M_burnerIn);
P02_P2=Equations.pressRatio_Isen(M_burnerIn);

AburnerIn_Ainlet = A2_Aystar/Ashock_Aystar*(Ashock_AC1/A1_C1);

%T_burnerIn = T_1*(1/T0y_T2)*T0y_Ty*(Py_Px/rhoy_rhox)*(1/T0x_Tx)*T01_1;

%% Burner Exit
% Mach number exiting burner/entering nozzle, given temperature and Mach #
% of flow entering burner, and required burner temperature
M_burnerOut = Equations.burnerMach(T_burnerIn,T_b,M_burnerIn)

% Assuming P2 = Pb, from conservation of mass and conservation of momentum,
% Ab/A2 relationship can be found
AburnerOut_AburnerIn = (gamma*M_burnerIn^2 + 1)/(gamma*M_burnerOut^2 + 1);
% Relate Ab to A1
Ab_A1 = AburnerOut_AburnerIn*AburnerIn_Ainlet;

%Temperature, Pressure, Density ratios at burner exit
T0b_Tb = Equations.tempRatio_Isen(M_burnerOut);

P0b_Pb = Equations.pressRatio_Isen(M_burnerOut);

rho0b_rhob = Equations.rhoRatio_Isen(M_burnerOut);

%% Nozzle Throat
%Astar represents the throat area assuming M=1
Ab_Abstar = Equations.areaRatio_Isen(M_burnerOut);

% Use area relations to calc throat area ratio to A1:
AC2_A1 = (1/Ab_Abstar)*(Ab_A1);

%% Nozzle Exhaust

%calc stag pressure vs pressure at stage 4
P04_P4 = P01_P1*(1/P0x_Px)*Py_Px*P0y_Py*(1/P02_P2)*P0b_Pb;

%find Mach that has this pressure ratio
M4 = sqrt((2/(gamma-1))*((P04_P4^((gamma-1)/gamma))-1));    

%stage 4 properties from isentropic relations
T04_T4 = Equations.tempRatio_Isen(M4); 
rho04_rho4 = Equations.rhoRatio_Isen(M4);
A4_Abstar = Equations.areaRatio_Isen(M4);

T4 = T_b * T0b_Tb / T04_T4;

% Use area relations to calc 4 section area ratio to A1:
A4_A1 = A4_Abstar*(AC2_A1);

%% Find A1, from ratios and give thrust required:
M4_M1_sq = (M4^2)/(M_inlet^2);
A1 = (Thrust/P_1)*((((gamma*M_inlet^2)*(M4_M1_sq*A4_A1-1))-A4_A1-1)^-1);

%% Find all area values from determined A1:
Areas = [1, (1/A1_C1), AburnerIn_Ainlet, Ab_A1, AC2_A1, A4_A1].*A1;

%% Thermodynamic Efficiency
% %test inputs: T1 = 210; T2 = 539; T3 = 600; T4 = 200;
% %for octane (can make this also an input?)
% Cp = 1.0045;
% eps = 44786;
% f_fa = 0.06641;
% 
% %if not octane:
% %x = ;
% %y = ;
% %f+fa = (12*x+y)/(137.3*(x+y/4));
% 
% eta_th = 1-(T4/T_b*((T_burnerIn/T_1)*((Cp*T_1)/(f_fa*eps))+1)-(Cp*T_1)/(f_fa*eps));

eta_th = 1-(T_1/T_burnerIn);

%% Propulsive Efficiency
%test input values: M1 = 2.8;M4 = 2.86;A4_A1 = 1.741;T1 = 210; U4 = 1478;
U4 = M4*sqrt(gamma*R*T4);
U1 = M_inlet*sqrt(gamma*R*T_1);
eta_prop = (gamma*M_inlet^2*((M4^2/M_inlet^2)*A4_A1-1))*((2*R*T_1)/(U4^2-U1^2)); 
%typo in slides?

%% Output values
A_inlet = Areas(1);
A_throat_i = Areas(2);
A_burn_in = Areas(3);
A_burn_ex = Areas(4);
A_throat_n = Areas(5);
A_exhaust = Areas(6);

end