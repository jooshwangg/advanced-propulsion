%%% Design of ramjet engine
clear all;
clc;
close all;

%% Which parameter do you want to vary?
choice = 7;
% 1: Flying altitude 
% 2: Flight mach number 
% 3: Normal shock strength
% 4: Burner entry mach number 
% 5: Burner temperature   
% 6: Required thrust

%% Define input variables
n = 100;        % Number of samples for sensitivity analysis [-]
% h             % Flying altitude [m]
h.min = 0;    
h.max = 45000;    
h.range = linspace(h.min, h.max, n)
h.avg = 20000;
[p_inf.avg, T_inf.avg] = atmosphere(h.avg);      % Calculate atmospheric properties for average height


% M_flight      % Flying Mach number [-]
M_flight.min = 0;
M_flight.max = 6;
M_flight.range = linspace(M_flight.min, M_flight.max, n);
M_flight.avg = 2;
% S_shock       % Normal shock strength [-]
S_shock.min = 1.01;
S_shock.max = 2;
S_shock.range = linspace(S_shock.min, S_shock.max, n);
S_shock.avg = 1.2;
% M_burn        % Burner Mach number [-]
M_burn.min = 0.1;
M_burn.max = 0.3;
M_burn.range = linspace(M_burn.min, M_burn.max, n);
M_burn.avg = 0.2;
% T_burn        % Burner temperature [K]
T_burn.min = 400;
T_burn.max = 2200;
T_burn.range = linspace(T_burn.min, T_burn.max, n);
T_burn.avg = 1700;
% T_req         % Thrust required [N]
T_req.min = 50000;
T_req.max = 200000;
T_req.range = linspace(T_req.min, T_req.max, n);
T_req.avg = 100000;
% T_burnIn        % Burner inlet temperature [K]
T_burnerIn.min = 300; %still need to change value
T_burnerIn.max = 600;
T_burnerIn.range = linspace(T_burnerIn.min, T_burnerIn.max, n);
T_burnerIn.avg = 500;


%% Define output variabels
% A_inlet       % Inlet area [m^2]
% A_throat_i    % Inlet throat area [m^2]
% A_burn_in     % Burner entry area [m^2]
% A_burn_ex     % Burner exit area [m^2]
% A_throat_n    % Nozzle throat area [m^2]
% A_exhaust     % Exhaust area [m^2]
% eta_th        % Thermodynamic efficiency [-]
% eta_prop      % Propulsive efficiency [-]
eta_th_range = zeros(1,n);
eta_prop_range = zeros(1,n);

%% Calculations
if choice == 1
    i = 0;          % Initialise counter
    for h_c = h.range
        i = i+1;    % Update counter
        
        % Calculate atmospheric properties
        [p_inf_c, T_inf_c] = atmosphere(h_c);
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf_c,T_inf_c,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn.avg,T_req.avg);
        
        eta_th_range(i)   = eta_th; 
        eta_prop_range(i) = eta_prop;
    end
end

if choice == 2
    i = 0;          % Initialise counter
    for M_flight_c = M_flight.range
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight_c,S_shock.avg,M_burn.avg,T_burn.avg,T_req.avg);
        
        eta_th_range(i)   = eta_th; 
        eta_prop_range(i) = eta_prop;
        
    end
end

if choice == 3
    i = 0;          % Initialise counter
    for S_shock_c = S_shock.range
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock_c,M_burn.avg,T_burn.avg,T_req.avg);
        
        eta_th_range(i)   = eta_th; 
        eta_prop_range(i) = eta_prop;
        
    end
end

if choice == 4
    i = 0;          % Initialise counter
    for M_burn_c = M_burn.range
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn_c,T_burn.avg,T_req.avg);
        
        eta_th_range(i)   = eta_th; 
        eta_prop_range(i) = eta_prop;
        
    end
end

if choice == 5
    i = 0;          % Initialise counter
    for T_burn_c = T_burn.range
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn_c,T_req.avg);
        
        eta_th_range(i)   = eta_th; 
        eta_prop_range(i) = eta_prop;

    end
end

if choice == 6
    i = 0;          % Initialise counter
    for T_req_c = T_req.range
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn.avg,T_req_c);
        
        eta_th_range(i)   = eta_th; 
        eta_prop_range(i) = eta_prop;

    end
end

if choice == 7 %slide page 4
    i = 0;          % Initialise counter
    for M_flight_c = M_flight.range
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop,T_1, M_burnerIn] = calculateRamjet_sw(p_inf.avg,T_inf.avg,...
            M_flight_c,S_shock.avg,T_burnerIn.avg,T_burn.avg,T_req.avg);

        M_burnerIn_range(i) = M_burnerIn;
    end
end

if choice == 8 %slide page 11
    i = 0;          % Initialise counter
    for T_burn_c = T_burn.range
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop,T_1] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn_c,T_req.avg);
        
        eta_cyc_range(i)   = 1-eta_th*eta_prop; %wrong side??

    end
end

if choice == 9 %slide page 20
    i = 0;          % Initialise counter
    for T_burn_c = T_burn.range
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop,T_1] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn_c,T_req.avg);
        
        % And this as output :
        %M_burn
        
    end
end

if choice == 10 %slide page 28 top
    j = 0;
    for M_flight = M_flight.avg:1:M_flight.avg+4
        j = j+1;
        i = 0;          % Initialise counter
        for T_burn_c = T_burn.range
            i = i+1;    % Update counter

            [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
                eta_th,eta_prop,T_1] = calculateRamjet(p_inf.avg,T_inf.avg,...
                M_flight,S_shock.avg,M_burn.avg,T_burn_c,T_req.avg);

            eta_prop_range(j,i)   = eta_prop; %wrong side??
        end
    end
end

if choice == 11 %slide page 28 bottom
    i = 0;          % Initialise counter
    for T_burn_c = T_burn.range %needs to be T_burnIn
        i = i+1;    % Update counter
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop,T_1] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn_c,T_req.avg);

        eta_th_range(i)    = eta_th;         
        eta_prop_range(i)  = eta_prop; 
        eta_cyc_range(i)   = 1-eta_th*eta_prop; %wrong side??
    end
end


%% Plot generation
if choice == 1
    figure()
    subplot(1,2,1)
    plot(h.range, eta_th_range)
    xlabel('h [m]')
    ylabel('\eta_{th} [-]')
    subplot(1,2,2)
    plot(h.range, eta_prop_range)
    xlabel('h [m]')
    ylabel('\eta_{prop} [-]')
end

if choice == 2
    figure()
    subplot(1,2,1)
    plot(M_flight.range, eta_th_range)
    xlabel('M_{flight} [-]')
    ylabel('\eta_{th} [-]')
    subplot(1,2,2)
    plot(M_flight.range, eta_prop_range)
    xlabel('M_{flight} [-]')
    ylabel('\eta_{prop} [-]')
end

if choice == 3
    figure()
    subplot(1,2,1)
    plot(S_shock.range, eta_th_range)
    xlabel('Shock strength [-]')
    ylabel('\eta_{th} [-]')
    subplot(1,2,2)
    plot(S_shock.range, eta_prop_range)
    xlabel('Shock strength [-]')
    ylabel('\eta_{prop} [-]')
end

if choice == 4
    figure()
    subplot(1,2,1)
    plot(M_burn.range, eta_th_range)
    xlabel('M_{burn} [-]')
    ylabel('\eta_{th} [-]')
    subplot(1,2,2)
    plot(M_burn.range, eta_prop_range)
    xlabel('M_{burn} [-]')
    ylabel('\eta_{prop} [-]')
end

if choice == 5
    figure()
    subplot(1,2,1)
    plot(T_burn.range, eta_th_range)
    xlabel('T_{burn} [K]')
    ylabel('\eta_{th} [-]')
    subplot(1,2,2)
    plot(T_burn.range, eta_prop_range)
    xlabel('T_{burn} [K]')
    ylabel('\eta_{prop} [-]')
end

if choice == 6
    figure()
    subplot(1,2,1)
    plot(T_req.range, eta_th_range)
    xlabel('T_{required} [N]')
    ylabel('\eta_{th} [-]')
    subplot(1,2,2)
    plot(T_req.range, eta_prop_range)
    xlabel('T_{required} [N]')
    ylabel('\eta_{prop} [-]')
end

if choice == 7
    figure()
    plot(M_flight.range, M_burnerIn.range)
    xlabel('M_{flight} [-]')
    ylabel('M_{burn} [-]') %slide 6
end

if choice == 8
    figure()
    plot(T_burn.range./T_1, eta_cyc_range)
    xlabel('T_{burn}/T_{1} [-]')
    ylabel('\eta_{cycle} [-]') %slide 6
end

if choice == 10
    figure()
    plot(T_burn.range./T_1, eta_prop_range)
    xlabel('T_{burn}/T_{1} [-]')
    ylabel('\eta_{prop} [-]') %slide 6
end