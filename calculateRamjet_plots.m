%%% Function description
% calculateRamjet_plots.m is used with main_plots.m, which generates extra
% plots (from Lecture 3 slides). Main differences between
% calculateRamjet_plots and caluclateRamjet are different output values,
% and the addition of a conditional statement for M_burnerIn. 

function [M_flight,M_burnerIn,M_burnerOut,T_inf,T_burnerIn,T_burn...
            ,eta_th,eta_prop,A4_A1,F_PA] = calculateRamjet_plots(p_inf,...
            T_inf,M_flight,S_shock,M_burn,T_burn,T_req)
%% Constants
gamma = 1.4;
R = 287;

%% Variables
% Condition for plot of burner entry Mach number versus inlet Mach number.
% When satisfied, the local function plot4() calculates the burner entry
% Mach number as a function of the flight Mach number.
if M_burn == -1 || M_burn == -2
    %Calculate M_burnerIn as function of M_flight
    M_burnerIn = plot4(M_burn,S_shock,M_flight);
else
    M_burnerIn = M_burn;
end

%% Flow properties at different stages in burner
% See Equations.m for isentropic flow and normal shock relations used to
% calculate flow properties

%% Inlet
%use isentropic relations to get property ratios
%Temperature ratio To1/T1 & Throat Temperature Ratio T01/T*
T01_1 = Equations.tempRatio_Isen(M_flight);
% %Pressure Ratio P01/P & Throat Pressure Ratio P01/PC1
P01_P1 = Equations.pressRatio_Isen(M_flight);
%Inlet Area Ratio (Isentropic Equation) [Ratio of Inlet to Throat]
A1_C1 = Equations.areaRatio_Isen(M_flight);

%% Throat to Shock
% where x is point immediately before shock and y is point immediately
% after shock

%use normal shock equations to get property ratios
%density ratio across normal shock of strength S_shock
rhoy_rhox = Equations.rhoRatio_NShock(S_shock);
%pressure ratio
Py_Px = Equations.pressRatio_NShock(S_shock);

%use isentropic equations to get property ratios
%area ratio between location of shock and inlet throat
Ashock_AC1 = Equations.areaRatio_Isen(S_shock);
%Pressure Ratio
P0x_Px = Equations.pressRatio_Isen(S_shock);
%temperature ratio 
T0x_Tx = Equations.tempRatio_Isen(S_shock);

%% Immediately after shock
%Mach number after shock
My = Equations.machNum_NShock(S_shock);

%use isentropic equations to get property ratios
%Ay* is area required to choke flow after shock
Ashock_Aystar = Equations.areaRatio_Isen(My); 
%Temperature Ratio
T0y_Ty = Equations.tempRatio_Isen(My);
%Pressure Ratio
P0y_Py=Equations.pressRatio_Isen(My);

%% Burner Entry
%use isentropic equations to get property ratios
A2_Aystar = Equations.areaRatio_Isen(M_burnerIn);
T0y_T2 = Equations.tempRatio_Isen(M_burnerIn);
P02_P2=Equations.pressRatio_Isen(M_burnerIn);

%use known area ratios to get sought after one
AburnerIn_Ainlet = A2_Aystar/Ashock_Aystar*(Ashock_AC1/A1_C1);
%use known temperature ratios to get temperature into burner
T_burnerIn = T_inf*(1/T0y_T2)*T0y_Ty*(Py_Px/rhoy_rhox)*(1/T0x_Tx)*T01_1;

%% Burner Exit
% Mach number exiting burner/entering nozzle, given temperature and Mach #
% of flow entering burner, and required burner temperature
M_burnerOut = Equations.burnerMach(T_burnerIn,T_burn,M_burnerIn);

% Assuming P2 = Pb, from conservation of mass and conservation of momentum,
% Ab/A2 relationship can be found
AburnerOut_AburnerIn = (gamma*M_burnerIn^2 + 1)/(gamma*M_burnerOut^2 + 1);
% Relate Ab to A1
Ab_A1 = AburnerOut_AburnerIn*AburnerIn_Ainlet;

%use isentropic equations to get property ratios
%Temperature, Pressure, Density ratios at burner exit
T0b_Tb = Equations.tempRatio_Isen(M_burnerOut);
P0b_Pb = Equations.pressRatio_Isen(M_burnerOut);

%% Nozzle Throat
%Astar represents the throat area assuming M=1
%use isentropic relations
Ab_Abstar = Equations.areaRatio_Isen(M_burnerOut);

% Use area relations to calc throat area ratio to A1:
AC2_A1 = (1/Ab_Abstar)*(Ab_A1);

%% Nozzle Exhaust

%calc stag pressure vs pressure at stage 4
P04_P4 = P01_P1*(1/P0x_Px)*Py_Px*P0y_Py*(1/P02_P2)*P0b_Pb;

%find Mach that has this pressure ratio
M4 = sqrt((2/(gamma-1))*((P04_P4^((gamma-1)/gamma))-1));    

%stage 4 properties from isentropic relations
T04_T4 = Equations.tempRatio_Isen(M4); 
A4_Abstar = Equations.areaRatio_Isen(M4);
T4 = T_burn * T0b_Tb / T04_T4;

% Use area relations to calc 4 section area ratio to A1:
A4_A1 = A4_Abstar*(AC2_A1);

%% Thermodynamic Efficiency
eta_th = 1-(T_inf/T_burnerIn);

%% Propulsive Efficiency
U4 = M4*sqrt(gamma*R*T4);
U1 = M_flight*sqrt(gamma*R*T_inf);
if U4>U1
    eta_prop = ((gamma*M_flight^2*((M4^2/M_flight^2)*A4_A1-1))+A4_A1-1)*((2*R*T_inf)/(U4^2-U1^2));
else
    eta_prop=NaN;
end
F_PA=(gamma*M_flight^2*((M4^2/M_flight^2)*A4_A1-1))+A4_A1-1;

end

% Find burner entry Mach number as a function of flight Mach number, and
% normal shock strength
function M_burnIn = plot4(M_burn,S_shock,M_flight)
    My = Equations.machNum_NShock(S_shock);
    T0y_Ty = Equations.tempRatio_Isen(My);
    Py_Px = Equations.pressRatio_NShock(S_shock);
    rhoy_rhox = Equations.rhoRatio_NShock(S_shock);
    T0x_Tx = Equations.tempRatio_Isen(S_shock);
    gamma = 1.4;
    Cshock = T0y_Ty*(Py_Px/rhoy_rhox)*(1/T0x_Tx);
    % Return minimum burner Mach number
    if M_burn == -1
        M_burnIn = sqrt((Cshock/7.14*(1+(gamma-1)/2*M_flight^2)-1)*(2/(gamma-1)));
    end
    % Return maximum burner Mach number
    if M_burn == -2
        M_burnIn = sqrt((Cshock/9.52*(1+(gamma-1)/2*M_flight^2)-1)*(2/(gamma-1)));
    end
end