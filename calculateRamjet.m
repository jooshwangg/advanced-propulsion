%% Function description
% Inputs
%     p_inf: free-stream pressure 
%     T_inf: free-stream temperature
%     M_flight: flight Mach number
%     S_shock: normal shock strength
%     M_burn: burner entry Mach number
%     T_burn: thrust required
% Outputs
%     A_inlet: ramjet inlet area (station 1 from Fig. 1 in report)
%     A_throat_i: compressor throat area (station C1)
%     A_burn_in: burner entry area (station 2)
%     A_burn_ex: burner exit area (station b)
%     A_throat_n: nozzle throat area (station C2)
%     A_exhaust: ramjet exhaust area (station 4)
%     eta_th: thermodynamic efficiency
%     eta_prop: propulsive efficiency

function [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust...
            ,eta_th,eta_prop] = calculateRamjet(p_inf,T_inf,M_flight,...
            S_shock,M_burn,T_burn,T_req)
       
%% Constants
gamma = 1.4;    %Assuming constant specific heat for dry air
R = 287;        %Assuming air is an ideal gas

%% Flow properties at different stages in burner
% See Equations.m for isentropic flow and normal shock relations used to
% calculate flow properties

%% Inlet
% use isentropic relations to get property ratios
% Temperature ratio To1/T1 & Throat Temperature Ratio T01/T*
T01_1 = Equations.tempRatio_Isen(M_flight);
% Pressure Ratio P01/P & Throat Pressure Ratio P01/PC1
P01_P1 = Equations.pressRatio_Isen(M_flight);
% Inlet Area Ratio (Isentropic Equation) [Ratio of Inlet to Throat]
A1_C1 = Equations.areaRatio_Isen(M_flight);

%% Throat to Shock
% Where x is point immediately before shock and y is point immediately
% after shock

%use normal shock equations to get property ratios
%density ratio across normal shock of strength S_shock
rhoy_rhox = Equations.rhoRatio_NShock(S_shock);
%pressure ratio
Py_Px = Equations.pressRatio_NShock(S_shock);

%use isentropic equations to get property ratios
%area ratio between location of shock and inlet throat
Ashock_AC1 = Equations.areaRatio_Isen(S_shock);
%Pressure Ratio
P0x_Px = Equations.pressRatio_Isen(S_shock);
%temperature ratio 
T0x_Tx = Equations.tempRatio_Isen(S_shock);

%% Immediately after shock
%Mach number after shock
My = Equations.machNum_NShock(S_shock);

%use isentropic equations to get property ratios
%Ay* is area required to choke flow after shock
Ashock_Aystar = Equations.areaRatio_Isen(My); 
%Temperature Ratio
T0y_Ty = Equations.tempRatio_Isen(My);
%Pressure Ratio
P0y_Py=Equations.pressRatio_Isen(My);

%% Burner Entry
%use isentropic equations to get property ratios
A2_Aystar = Equations.areaRatio_Isen(M_burn);
T0y_T2 = Equations.tempRatio_Isen(M_burn);
P02_P2=Equations.pressRatio_Isen(M_burn);

% use known area ratios to get sought after one
AburnerIn_Ainlet = A2_Aystar/Ashock_Aystar*(Ashock_AC1/A1_C1);
% use known temperature ratios to get temperature into burner
T_burnerIn = T_inf*(1/T0y_T2)*T0y_Ty*(Py_Px/rhoy_rhox)*(1/T0x_Tx)*T01_1;

%% Burner Exit
% Mach number exiting burner/entering nozzle, given temperature and Mach #
% of flow entering burner, and required burner temperature
M_burnerOut = Equations.burnerMach(T_burnerIn,T_burn,M_burn);

% Assuming P2 = Pb, from conservation of mass and conservation of momentum,
% Ab/A2 relationship can be found
AburnerOut_AburnerIn = (gamma*M_burn^2 + 1)/(gamma*M_burnerOut^2 + 1);
% Relate Ab to A1
Ab_A1 = AburnerOut_AburnerIn*AburnerIn_Ainlet;

% use isentropic equations to get property ratios
% Temperature, Pressure, Density ratios at burner exit
T0b_Tb = Equations.tempRatio_Isen(M_burnerOut);
P0b_Pb = Equations.pressRatio_Isen(M_burnerOut);

%% Nozzle Throat
% Astar represents the throat area assuming M=1
% use isentropic relations
Ab_Abstar = Equations.areaRatio_Isen(M_burnerOut);

% Use area relations to calc throat area ratio to A1:
AC2_A1 = (1/Ab_Abstar)*(Ab_A1);

%% Nozzle Exhaust
% calculate stagnation pressure vs pressure at stage 4
P04_P4 = P01_P1*(1/P0x_Px)*Py_Px*P0y_Py*(1/P02_P2)*P0b_Pb;

%find Mach that has this pressure ratio
M4 = sqrt((2/(gamma-1))*((P04_P4^((gamma-1)/gamma))-1));    

%stage 4 properties from isentropic relations
T04_T4 = Equations.tempRatio_Isen(M4); 
A4_Abstar = Equations.areaRatio_Isen(M4);
T4 = T_burn * T0b_Tb / T04_T4;

% Use area relations to calc 4 section area ratio to A1:
A4_A1 = A4_Abstar*(AC2_A1);

%% Find A1, from ratios and give thrust required:
M4_M1_sq = (M4^2)/(M_flight^2);
A1 = (T_req/p_inf)*((((gamma*M_flight^2)*(M4_M1_sq*A4_A1-1))+A4_A1-1)^-1);

%% Find all area values from determined A1:
Areas = [1, (1/A1_C1), AburnerIn_Ainlet, Ab_A1, AC2_A1, A4_A1].*A1;

%% Thermodynamic Efficiency
% Ratio needed for the calculation of T_A
P2_P1=1/(P02_P2*(1/P0y_Py)*(1/Py_Px)*P0x_Px*(1/P01_P1));
% Calculate the temperature at which T_inf would be when there would be no
% shock losses
T_A=T_burnerIn/((P2_P1)^((gamma-1)/gamma)); 
% Calculation of the compressive efficiency loss due to the shock
eta_c=(T_burnerIn-T_A)/(T_burnerIn-T_inf);

% For this calculation, the properties of octane are used
Cp = 1.0045;
eps = 44786;
f_fa = 0.06641;

% Cycle efficiency including shocklosses and asumming isentropic expansion
eta_th = (Cp*T_inf)/(f_fa*eps)*(T_burnerIn/T_inf-1)*(eta_c*...
    (1+(T_inf/T_burnerIn)*((f_fa*eps)/(Cp*T_inf)))-1);

%% Propulsive Efficiency
% exhaust velocity
U4 = M4*sqrt(gamma*R*T4);
% inlet velocity
U1 = M_flight*sqrt(gamma*R*T_inf);
eta_prop = ((gamma*M_flight^2*((M4^2/M_flight^2)*A4_A1-1))+A4_A1-1)*...
    ((2*R*T_inf)/(U4^2-U1^2)); 

%% Output values
A_inlet = Areas(1);
A_throat_i = Areas(2);
A_burn_in = Areas(3);
A_burn_ex = Areas(4);
A_throat_n = Areas(5);
A_exhaust = Areas(6);

end