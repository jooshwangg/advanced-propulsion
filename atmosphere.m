function [p,T] = atmosphere(h)
% Atmosphere calculates the air properties in the atmosphere
%% Inputs
% h:    height in the standard atmosphere [m]

%% Outputs
% p:    pressure [Pa]
% T:    temperature [K]

%% Define standard atmosphere parameters
p_0     = 101325;       % Sea-level pressure [Pa]
T_0     = 288.15;       % Sea-level temperature [K]

T_11    = 216.65;       % Temperature at 11000m [K]
p_11    = 22632.1;      % Pressure at 11000m [Pa]

T_20    = 216.65;       % Temperature at 20000m [K]       
p_20    = 5474.89;      % Pressure at 20000m [Pa]

T_32    = 228.65;       % Temperature at 32000m [K]
p_32    = 868.019;      % Pressure at 32000m [Pa]

L_ts    = 0.0065;       % Lapse-rate of the troposphere [K/m]
L_tp    = 0.0;          % Lapse-rate tropopause/stratosphere 1 [K/m]
L_ss1   = -0.001;       % Lapse-rate stratosphere 1 [K/m]
L_ss2   = -0.0028;      % Lapse-rate stratosphere 2/stratopause [K/m]

g       = 9.80665;      % Gravitational acceleration [m/s^2]
R       = 287;          % Perfect gas constant [J/kgK]

%% Calculations for different levels (roughly) of US standard atmosphere
% Troposphere
if h < 11000
    T = T_0 - L_ts*h;
    p = p_0 * (T/T_0)^(g/L_ts/R);
% Tropopause     
elseif h >= 11000 && h < 20000
    T = T_11;
    p = p_11*exp(g*(11000-h)/R/T_11);
% Stratosphere
elseif h >= 20000 && h < 32000
    T = T_20 - L_ss1*(h-20000);
    p = p_20 * (T/T_20)^(g/L_ss1/R);
% Stratopause
elseif h >= 32000 && h < 47000
    T = T_32 - L_ss2*(h-32000);
    p = p_32 * (T/T_32)^(g/L_ss2/R);
end

end