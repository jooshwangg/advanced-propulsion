function [A_burn_in,A_burn_out] = calculateBurnerEntryArea(...
    T1,M1,M_shock,M_burnerIn,T_burner,A_inlet,A_throat_i)
% Test values (from example in lecture 3)
% T1 = 210; K, free stream temp
% M1 = 2.8; flight Mach number
% M_shock = 1.1; shock strength
% M_burnerEntry = 0.2; Mach number in burner
% T_burner = 1700: K, Temperature required in burner

% AC1_Ainlet = A_throat_i/A_inlet;
AC1_Ainlet = 0.2869; %should pull value from previous stage

% JP7 heat of combustion: 43.5 MJ/kg
% JP7 flashpoint: 60 C
% Boeing X51 used JP7 at 290C in engines

%% Assumptions:
%   P_2 = P_b
%   Isentropic conditions in burner

gamma = 1.4;

%% Flow properties at different stages in burner
% See below for isentropic flow and normal shock relations used to
% calculate flow properties

%% Inlet

T0x_T1 = tempRatio_Isen(M1);

%% Across shock
% where x is point immediately before shock and y is point immediately
% after shock
%density ratio across normal shock of strength M_shock
rhoy_rhox = rhoRatio_NShock(M_shock);
%pressure ratio
Py_Px = pressRatio_NShock(M_shock);
%area ratio between location of shock and inlet throat
Ashock_AC1 = areaRatio_Isen(M_shock);
%temperature ratio 
T0x_Tx = tempRatio_Isen(M_shock);

%% Immediately after shock
My = machNum_NShock(M_shock);
Ashock_Aystar = areaRatio_Isen(My); % Ay* is area required to choke flow after shock
T0y_Ty = tempRatio_Isen(My);

%% Burner Entry
A2_Aystar = areaRatio_Isen(M_burnerIn);
T0y_T2 = tempRatio_Isen(M_burnerIn);

AburnerIn_Ainlet = A2_Aystar/Ashock_Aystar*(Ashock_AC1*AC1_Ainlet);

T_burnerIn = T1*(1/T0y_T2)*T0y_Ty*(Py_Px/rhoy_rhox)*(1/T0x_Tx)*T0x_T1;

%% Burner Exit
% Mach number exiting burner/entering nozzle, given temperature and Mach #
% of flow entering burner, and required burner temperature
M_burnerOut = burnerMach(T_burnerIn,T_burner,M_burnerIn);

% Assuming P2 = Pb, from conservation of mass and conservation of momentum,
% Ab/A2 relationship can be found
AburnerOut_AburnerIn = (gamma*M_burnerIn^2 + 1)/(gamma*M_burnerOut^2 + 1);


%% Main outputs
 A_burn_in = AburnerIn_Ainlet*A_inlet;
 A_burn_out = AburnerOut_AburnerIn*A_burn_in;

%% Isentropic flow relations
    function A_Astar = areaRatio_Isen(M)
        A_Astar = (1/M)*((2/(gamma+1))*(1+((gamma-1)/2)*M^2))^((gamma+1)/(2*(gamma-1)));
    end

    function T0_T = tempRatio_Isen(M)
        T0_T = 1 + ((gamma-1)/2)*M^2;
    end

%% Normal shock relations
    function M2 = machNum_NShock(M1)
        M2 = sqrt(((gamma-1)*M1^2+2)/(2*gamma*M1^2-(gamma-1)));
    end

    function P2_P1 = pressRatio_NShock(M)
        P2_P1 = (2*gamma*M^2-(gamma-1)) / (gamma+1);
    end

    function rho2_rho1 = rhoRatio_NShock(M)
        rho2_rho1 = ((gamma+1)*M^2) / ((gamma-1)*M^2 + 2);
    end

%% Burner Mach number quadratic solver
    function Mb = burnerMach(T2,Tb,M2)
        C1 = T2/Tb;
        C2 = (M2+1/(gamma*M2));
        Mb1 = 0.5*sqrt(C1)*(C2) + 0.5*sqrt((C1)*(C2^2)-4/gamma);
        Mb2 = 0.5*sqrt(C1)*(C2) - 0.5*sqrt((C1)*(C2^2)-4/gamma);
        noChoke = [Mb1, Mb2];
        Mb = noChoke(noChoke<1 & noChoke>0);
    end

end