% run('variables.m')
clear
%INPUTS
gamma=1.4;          %const
M_1=2.8;       %var
P_1=70*10^3;    %var?
T_1=210;
T_b=1700;
M_x=1.1;  %'good' pre burner shock
M_2=0.2;       %var
Thrust=1000;
M_4=2.86;

%% Inlet
%Temperature ratio To1/T1 & Throat Temperature Ratio T01/T*
T_01_1=isen_t_ratio(M_1, gamma);
T_01_C1=isen_t_ratio(1, gamma);

% %Pressure Ratio P01/P & Throat Pressure Ratio P01/PC1
P_01_1=isen_p_ratio(M_1, gamma);
P_01_C1=isen_p_ratio(1, gamma);

%Inlet Area Ratio (Isentropic Equation) [Ratio of Inlet to Throat]
A_1_C1=isen_a_ratio(M_1, gamma);

A_1=1;
%% Throat
%Throat Area
A_throat=A_1/A_1_C1;
%Throat Pressure
 P_C1=P_1*P_01_1/P_01_C1;
%Throat Temperature
T_C1=T_1*T_01_1/T_01_C1;

%% Throat to Shock
%Area ratio [isentropic] required to get to this M
A_s_C1=isen_a_ratio(M_x, gamma);
%Pressure Ratio
P_01_x=isen_p_ratio(M_x, gamma);
%Temperature Ratio
T_01_s=isen_t_ratio(M_x, gamma);

%% Post Throat Shock
%Calculating burner inlet Mach number using 'good' shock strength (1.1)
M_y=normalshock_M(M_x, gamma);
%Pressure Ratio Py/Px
P_y_x=normalshock_p_ratio(M_x, gamma);
%Temperature Ratio T
T_0x_x=isen_t_ratio(M_x, gamma);
%density Ratio
rho_y_x=normalshock_rho_ratio(M_x, gamma);

%% Burner Inlet
%Area ratio 
A_s_y=isen_a_ratio(M_y, gamma);
%Temperature Ratio, after shock
T_0y_y=isen_t_ratio(M_y, gamma);
%Temperature Ratio, burner inlet
T_02_2=isen_t_ratio(M_2, gamma);
%Pressure Ratio at burner inlet
P_02_2=isen_p_ratio(M_2, gamma);
%Pressure Ratio after shock
P_02_y=isen_p_ratio(M_y, gamma);

%Burner Area Ratio [required to go from M post shock to required burner Mach]
A_2_y=isen_a_ratio(M_2, gamma);

%Area ratio A1 (inlet) to A2 (beginning of burner)
A_2_1=A_2_y*(1/A_s_y)*A_s_C1*(1/A_1_C1);


%temperature 
%T2=T1*   T2/T0y   * T0y/Ty  * (Ty/Tx) * Tx/T0x * T0x/T1
%T2=T1*   T2/T0y   * T0y/Ty  * (Py/Px * px/py) * Tx/T0x * T0x/T1
T_2_1=(1/T_02_2)*(T_0y_y)*(P_y_x * (1/rho_y_x))      *(1/T_0x_x)*  T_01_1;
T_2=T_1 *T_2_1;

%% Burner 

%Burner exit Mach
M_b=0.5*((T_2/T_b)^0.5)*(M_2+1/(gamma*M_2))-0.5*((T_2/T_b)*((M_2+1/(gamma*M_2))^2)-4/gamma)^0.5;
%Burner Area Ratio 2-B
A_b_2=(gamma*M_2^2+1)/(gamma*M_b^2+1);

%Area Ratio 1-B
A_b_1=A_b_2*A_2_1;

%% Nozzle
%Area ratio - burner to throat
A_b_C2=isen_a_ratio(M_b, gamma);
%Pressure Ratio
P_0b_2=isen_p_ratio(M_b, gamma);
%Temperature Ratio
T_0b_b=isen_t_ratio(M_b, gamma);

%Area Ratio from 1 to 2nd throat
A_C2_1=(1/A_b_C2)*A_b_1;

% Stagnation pressure ratio at exit
P_04_4=P_0b_2 * (1/P_02_2) * P_02_y * P_y_x * (1/P_01_x) * P_01_1;

%Calculate Exit Mach Number
M_4=((2/(gamma-1))*(P_04_4^((gamma-1)/gamma)-1))^0.5;


%Temperature Ratio
T_04_4=isen_t_ratio(M_4, gamma);
%Area Ratio C2-4
A_4_C2=isen_a_ratio(M_4, gamma);

%Full Area Ratio
A_4_1=A_4_C2*A_C2_1;

%Final Temperature
T_4_b=T_0b_b*(1/T_04_4);
T_4=T_b*T_4_b;


%% Performance And Final Areas

%Inlet Area
A_1=Thrust/(P_1*gamma*(M_1^2)*(((M_4^2)/M_1^2)*(A_4_1)-1));
