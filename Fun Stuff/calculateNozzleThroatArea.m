function AC2_A1= calculateNozzleThroatArea(Mb,Ab_A1)
%% Assumptions:
%   Isentropic conditions

% %% Test with examples from Lecture 3
% M1=2.8; Mx=1.1; My=0.91; M2=0.2; Mb=0.42; F=1000; P1=70*1000; AC2_A1=0.4719; Ab_A1=0.7215; A2_A1=0.8519; AC1_A1=0.2869;

%% Find ratios from Mb (mach number at exit of burner, determined previosly):

T0b_Tb=Equations.tempRatio_Isen(Mb);

P0b_Pb=Equations.pressRatio_Isen(Mb);

rho0b_rhob=Equations.rhoRatio_Isen(Mb);

%Astar represents the throat area assuming M=1
Ab_Abstar=Equations.areaRatio_Isen(Mb);

%% Use area relations to calc throat area ratio to A1:
AC2_A1=(1/Ab_Abstar)*(Ab_A1);


end