function Areas= calculateExhaustArea(F,P1,M1,Mx,My,M2,Mb,AC1_A1, A2_A1, Ab_A1, AC2_A1, A4_A1)
%% Assumptions:
%   P_1 = P_4
%   Isentropic conditions
gamma=Equations.gamma;

% %% Test with examples from Lecture 3
% M1=2.8; Mx=1.1; My=0.91; M2=0.2; Mb=0.42; F=1000; P1=70*1000; AC2_A1=0.4719; Ab_A1=0.7215; A2_A1=0.8519; AC1_A1=0.2869;

%% Pressure ratios from each stage to determine stage 4
P01_P1=Equations.pressRatio_Isen(M1);

P0x_Px=Equations.pressRatio_Isen(Mx);

Py_Px=Equations.pressRatio_NShock(Mx);

P0y_Py=Equations.pressRatio_Isen(My);

P02_P2=Equations.pressRatio_Isen(M2);

P0b_Pb=Equations.pressRatio_Isen(Mb);


%calc stag pressure vs pressure at stage 4
P04_P4=P01_P1*(1/P0x_Px)*Py_Px*P0y_Py*(1/P02_P2)*P0b_Pb;

%find Mach that has this pressure ratio

M4=sqrt((2/(gamma-1))*((P04_P4^((gamma-1)/gamma))-1));    

%%stage 4 properties from isentropic relations
T04_T4=Equations.tempRatio_Isen(M4); 
rho04_rho4=Equations.rhoRatio_Isen(M4);
A4_Abstar=Equations.areaRatio_Isen(M4);

%% Use area relations to calc 4 section area ratio to A1:
A4_A1=A4_Abstar*(AC2_A1);


%% Find A1, from ratios and give thrust required:
M4_M1_sq=(M4^2)/(M1^2);
A1=(F/P1)*(((gamma*M1^2)*(M4_M1_sq*A4_A1-1))^-1);

%% Find all area values from determined A1:
Areas=[1, AC1_A1, A2_A1, Ab_A1, AC2_A1, A4_A1].*A1;


end