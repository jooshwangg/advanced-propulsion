function [A_burn_in,A_burn_out] = calculateBurnerAreas(...
    T1,M1,M_shock,M_burnerIn,T_burner,A_inlet,A_throat_i)
% Test values (from example in lecture 3)
% T1 = 210; K, free stream temp
% M1 = 2.8; flight Mach number
% M_shock = 1.1; shock strength
% M_burnerEntry = 0.2; Mach number in burner
% T_burner = 1700: K, Temperature required in burner
% A_inlet = 1;
% A_throat_i = 0.2869;

AC1_Ainlet = A_throat_i/A_inlet;

% JP7 heat of combustion: 43.5 MJ/kg
% JP7 flashpoint: 60 C
% Boeing X51 used JP7 at 290C in engines

%% Assumptions:
%   P_2 = P_b
%   Isentropic conditions in burner

gamma = 1.4;

%% Flow properties at different stages in burner
% See below for isentropic flow and normal shock relations used to
% calculate flow properties

%% Inlet

T0x_T1 = Equations.tempRatio_Isen(M1);

%% Across shock
% where x is point immediately before shock and y is point immediately
% after shock
%density ratio across normal shock of strength M_shock
rhoy_rhox = Equations.rhoRatio_NShock(M_shock);
%pressure ratio
Py_Px = Equations.pressRatio_NShock(M_shock);
%area ratio between location of shock and inlet throat
Ashock_AC1 = Equations.areaRatio_Isen(M_shock);
%temperature ratio 
T0x_Tx = Equations.tempRatio_Isen(M_shock);

%% Immediately after shock
%My = machNum_NShock(M_shock);
My = Equations.machNum_NShock(M_shock);
Ashock_Aystar = Equations.areaRatio_Isen(My); % Ay* is area required to choke flow after shock
T0y_Ty = Equations.tempRatio_Isen(My);

%% Burner Entry
A2_Aystar = Equations.areaRatio_Isen(M_burnerIn);
T0y_T2 = Equations.tempRatio_Isen(M_burnerIn);

AburnerIn_Ainlet = A2_Aystar/Ashock_Aystar*(Ashock_AC1*AC1_Ainlet);

T_burnerIn = T1*(1/T0y_T2)*T0y_Ty*(Py_Px/rhoy_rhox)*(1/T0x_Tx)*T0x_T1;

%% Burner Exit
% Mach number exiting burner/entering nozzle, given temperature and Mach #
% of flow entering burner, and required burner temperature
M_burnerOut = Equations.burnerMach(T_burnerIn,T_burner,M_burnerIn);

% Assuming P2 = Pb, from conservation of mass and conservation of momentum,
% Ab/A2 relationship can be found
AburnerOut_AburnerIn = (gamma*M_burnerIn^2 + 1)/(gamma*M_burnerOut^2 + 1);


%% Main outputs
 A_burn_in = AburnerIn_Ainlet*A_inlet;
 A_burn_out = AburnerOut_AburnerIn*A_burn_in;

end