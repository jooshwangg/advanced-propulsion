function eta_prop = calculatePropEfficiency(M1,M4,A4_A1,T1,T4) %ask if variables are correct

%test input values:  M1 = 2.8;M4 = 2.86;A4_A1 = 1.741;T1 = 210;U4 = 1478;U1 = M1*sqrt(gamma*R*T1);
gamma = 1.4;
R = 287;
eta_prop = (gamma*M1^2*((M4^2/M1^2)*A4_A1-1))*((2*R*T1)/(U4^2-U1^2)); %typo in slides?

end
