function eta_th = calculateThermoEfficiency(T1,T2,T3,T4) %ask if variables are correct

%test inputs: T1 = 210; T2 = 539; T3 = 600; T4 = 200;

%for octane (can make this also an input?)
Cp = 1.0045;
eps = 44786;
f_fa = 0.06641;

%if not octane:
%x = ;
%y = ;
%f+fa = (12*x+y)/(137.3*(x+y/4));

eta_th = 1-(T4/T3*((T2/T1)*((Cp*T1)/(f_fa*eps))+1)-(Cp*T1)/(f_fa*eps))

end

