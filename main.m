%%% Design of ramjet engine
clear, clc
close all

%% Variation parameter
% Value of variable 'choice' determines which parameter is varied

choice = 3;

% 1: Flying altitude - h (determines p_inf and T_inf)
% 2: Flight mach number - M_flight
% 3: Normal shock strength - S_shock
% 4: Burner entry mach number - M_burn
% 5: Burner temperature - T_burn
% 6: Required thrust - T_req

%% Define input variables
n = 100;        % Number of samples for analysis [-]

% h             % Flying altitude [m]
h.min = 0;    
h.max = 27432;    
h.range = linspace(h.min, h.max, n);
h.avg = 24000;
% Calculate atmospheric properties for average height
[p_inf.avg, T_inf.avg] = atmosphere(h.avg);    

% M_flight      % Flying Mach number [-]
M_flight.min = 1;
M_flight.max = 5;
M_flight.range = linspace(M_flight.min, M_flight.max, n);
M_flight.avg = 3.2;

% S_shock       % Normal shock strength [-]
S_shock.min = 1.01;
S_shock.max = 3.5;
S_shock.range = linspace(S_shock.min, S_shock.max, n);
S_shock.avg = 1.1;

% M_burn        % Burner entry Mach number [-]
M_burn.min = 0.1;
M_burn.max = 0.3;
M_burn.range = linspace(M_burn.min, M_burn.max, n);
M_burn.avg = 0.2;

% T_burn        % Burner temperature [K]
T_burn.min = 1000;
T_burn.max = 2200;
T_burn.range = linspace(T_burn.min, T_burn.max, n);
T_burn.avg = 1500;

% T_req         % Thrust required [N]
T_req.min = 0;
T_req.max = 150000;
T_req.range = linspace(T_req.min, T_req.max, n);
T_req.avg = 100000;

%% Define output variables
% A_inlet       % Inlet area [m^2]
% A_throat_i    % Inlet throat area [m^2]
% A_burn_in     % Burner entry area [m^2]
% A_burn_ex     % Burner exit area [m^2]
% A_throat_n    % Nozzle throat area [m^2]
% A_exhaust     % Exhaust area [m^2]
% eta_th        % Thermodynamic efficiency [-]
% eta_prop      % Propulsive efficiency [-]

% Define column names of output variables
outputVars = {'InletArea','InletThroatArea','BurnerEntryArea',...
            'BurnerExitArea','NozzleThroatArea','ExhaustArea',...
            'ThermoEff','PropEff'};
% Matrix to hold output values, with first column in matrix to hold varied 
% parameter   
outputVals = zeros(n,9);
        
%% Calculations
% Each code block for a given value of 'choice' calculates ramjet areas and
% efficiencies for a range of the selected parameter, while holding other
% values constant at predetermined standard flight conditions (*.avg
% values). The outputs of the calculateRamjet function are then written,
% with the varied parameter, to a matrix to be formatted into a table (for
% easier examining of specific values in Matlab).

% Flight altitude, h [m]
if choice == 1
    i = 0;          % Initialise counter
    for h_c = h.range
        i = i+1;    % Update counter
        
        % Calculate atmospheric properties dependant on flight altitude
        [p_inf_c, T_inf_c] = atmosphere(h_c);
        %p_inf_c = p_inf_c*9;
        %T_inf_c = T_inf_c*2.5;
        
        % Calculate ramjet areas and related efficiencies
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf_c,T_inf_c,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn.avg,T_req.avg);
        
        % Write all outputs to matrix, plus independent variables, to be
        % written to table
        outputVals(i,:) = [h_c,A_inlet,A_throat_i,A_burn_in,A_burn_ex,...
            A_throat_n,A_exhaust,eta_th,eta_prop];
    end
    
    % Add independent variable name to array of output variable names
    outputVars = cat(2,{'Height'},outputVars);
end

% Flight Mach number, M_flight [-]
if choice == 2
    i = 0;          
    for M_flight_c = M_flight.range
        i = i+1;    
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight_c,S_shock.avg,M_burn.avg,T_burn.avg,T_req.avg);

        outputVals(i,:) = [M_flight_c,A_inlet,A_throat_i,A_burn_in,...
            A_burn_ex,A_throat_n,A_exhaust,eta_th,eta_prop];
    end   
    outputVars = cat(2,{'FlightMach'},outputVars);
end

% Normal shock strength, S_shock [-]
if choice == 3
    i = 0;
    for S_shock_c = S_shock.range
        i = i+1;   
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock_c,M_burn.avg,T_burn.avg,T_req.avg);
        
        outputVals(i,:) = [S_shock_c,A_inlet,A_throat_i,A_burn_in,...
            A_burn_ex,A_throat_n,A_exhaust,eta_th,eta_prop];
    end   
    outputVars = cat(2,{'ShockStr'},outputVars);
end

% Burner entry Mach number, M_burn [-]
if choice == 4
    i = 0;        
    for M_burn_c = M_burn.range
        i = i+1;   
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn_c,T_burn.avg,T_req.avg);

        outputVals(i,:) = [M_burn_c,A_inlet,A_throat_i,A_burn_in,...
            A_burn_ex,A_throat_n,A_exhaust,eta_th,eta_prop];
    end
    outputVars = cat(2,{'BurnerMach'},outputVars);
end

% Burner combustion temperature, T_burn [K]
if choice == 5
    i = 0;          
    for T_burn_c = T_burn.range
        i = i+1;   
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn_c,T_req.avg);

        outputVals(i,:) = [T_burn_c,A_inlet,A_throat_i,A_burn_in,...
            A_burn_ex,A_throat_n,A_exhaust,eta_th,eta_prop];
    end
    outputVars = cat(2,{'BurnerTemp'},outputVars);
end

% Thrust required, T_req [N]
if choice == 6
    i = 0;          
    for T_req_c = T_req.range
        i = i+1;    
        
        [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,...
            eta_th,eta_prop] = calculateRamjet(p_inf.avg,T_inf.avg,...
            M_flight.avg,S_shock.avg,M_burn.avg,T_burn.avg,T_req_c);

        outputVals(i,:) = [T_req_c,A_inlet,A_throat_i,A_burn_in,...
            A_burn_ex,A_throat_n,A_exhaust,eta_th,eta_prop];
    end
    outputVars = cat(2,{'Thrust'},outputVars);
end

%% Plot generation (for choice 1-6)
orient landscape
%plot thermodynamic efficiency

% For each value of 'choice', which represents a different varied input
% property, 3 plots are generated as subplots in a single figure. Each
% subplot is plotted against the variable of interest. The three subplots
% are (in order): thermodynamic/cycle efficiency, propulsive efficiency,
% and 6 critical radii of the ramjet.

% Flight altitude, h [m]
if choice == 1
    % Plot thermodynamic efficiency versus independent variable
    subplot(1,3,1)
    plot(outputVals(:,1), round(outputVals(:,8),2))
    xlabel('h [m]')
    ylabel('\eta_{th} [-]')
    xlim([h.min h.max])
    ylim([0 1])
    set(gca,'FontSize',15)
    
    % Plot propulsive efficiency versus independent variable
    subplot(1,3,2)
    plot(outputVals(:,1), outputVals(:,9))
    xlabel('h [m]')
    ylabel('\eta_{prop} [-]')
    xlim([h.min h.max])
    set(gca,'FontSize',15)
    
    % Plot ramjet station radii versus independent variable
    subplot(1,3,3)
    plot(outputVals(:,1),sqrt(outputVals(:,2:7)/pi))
    xlabel('h [m]')
    ylabel('Radius [m]')
    xlim([h.min h.max])
    set(gca,'FontSize',15)
    legend(outputVars(2:7),'Location','northwest')
end

% Flight Mach number, M_flight [-]
if choice == 2
    subplot(1,3,1)
    plot(outputVals(:,1), outputVals(:,8))
    xlabel('M_{flight} [-]')
    ylabel('\eta_{th} [-]')
    xlim([M_flight.min M_flight.max])
    set(gca,'FontSize',15)
    
    subplot(1,3,2)
    plot(outputVals(:,1), outputVals(:,9))
    xlabel('M_{flight} [-]')
    ylabel('\eta_{prop} [-]')
    xlim([M_flight.min M_flight.max])
    set(gca,'FontSize',15)
    
    subplot(1,3,3)
    plot(outputVals(:,1),sqrt(outputVals(:,2:7)/pi))
    xlabel('M_{flight} [-]')
    ylabel('Radius [m]')
    xlim([M_flight.min M_flight.max])
    set(gca,'FontSize',15)
    legend(outputVars(2:7),'Location','northwest')
end

% Normal shock strength, S_shock [-]
if choice == 3
    subplot(1,3,1)
    plot(outputVals(:,1), outputVals(:,8))
    xlabel('Shock strength [-]')
    ylabel('\eta_{th} [-]')
    xlim([S_shock.min S_shock.max])
    ylim([0 1])
    set(gca,'FontSize',15)
    
    subplot(1,3,2)
    plot(outputVals(:,1), outputVals(:,9))
    xlabel('Shock strength [-]')
    ylabel('\eta_{prop} [-]')
    xlim([S_shock.min S_shock.max])
    ylim([0 1])
    set(gca,'FontSize',15)
    
    subplot(1,3,3)
    plot(outputVals(:,1),sqrt(outputVals(:,2:7)/pi))
    xlabel('Shock strength [-]')
    ylabel('Radius [m]')
    xlim([S_shock.min S_shock.max])
    set(gca,'FontSize',15)
    legend(outputVars(2:7),'Location','northwest')
end

% Burner entry Mach number, M_burn [-]
if choice == 4
    subplot(1,3,1)
    plot(outputVals(:,1), outputVals(:,8))
    xlabel('M_{2} [-]')
    ylabel('\eta_{th} [-]')
    set(gca,'FontSize',15)
    
    subplot(1,3,2)
    plot(outputVals(:,1), outputVals(:,9))
    xlabel('M_{2} [-]')
    ylabel('\eta_{prop} [-]')
    set(gca,'FontSize',15)
    
    subplot(1,3,3)
    plot(outputVals(:,1),sqrt(outputVals(:,2:7)/pi))
    xlabel('M_{2} [-]')
    ylabel('Radius [m]')
    set(gca,'FontSize',15)
    legend(outputVars(2:7),'Location','northeast')  
end

% Burner combustion temperature, T_burn [K]
if choice == 5
    subplot(1,3,1)
    plot(outputVals(:,1), outputVals(:,8))
    xlabel('T_{burn} [K]')
    ylabel('\eta_{th} [-]')
    xlim([T_burn.min T_burn.max])
    set(gca,'FontSize',15)
    
    subplot(1,3,2)
    plot(outputVals(:,1), outputVals(:,9))
    xlabel('T_{burn} [K]')
    ylabel('\eta_{prop} [-]')
    xlim([T_burn.min T_burn.max])
    set(gca,'FontSize',15)
    
    subplot(1,3,3)
    plot(outputVals(:,1),sqrt(outputVals(:,2:7)/pi))
    xlabel('T_{burn} [K]')
    ylabel('Radius [m]')
    xlim([T_burn.min T_burn.max])
    set(gca,'FontSize',15)
    legend(outputVars(2:7),'Location','northeast') 
end

% Thrust required, T_req [N]
if choice == 6
    subplot(1,3,1)
    plot(outputVals(:,1), outputVals(:,8))
    xlabel('T_{required} [N]')
    ylabel('\eta_{th} [-]')
    set(gca,'FontSize',15)
    
    subplot(1,3,2)
    plot(outputVals(:,1), outputVals(:,9))
    xlabel('T_{required} [N]')
    ylabel('\eta_{prop} [-]')
    set(gca,'FontSize',15)
    
    subplot(1,3,3)
    plot(outputVals(:,1),sqrt(outputVals(:,2:7)/pi))
    xlabel('T_{required} [N]')
    ylabel('Radius [m]')
    set(gca,'FontSize',15)
    legend(outputVars(2:7),'Location','northwest') 
end

%% Table Generation
% Create table: first column is varied input, rest of column values are
% [A_inlet,A_throat_i,A_burn_in,A_burn_ex,A_throat_n,A_exhaust,eta_th,...
% eta_prop]
Table = table(0,0,0,0,0,0,0,0,0);
Table = repmat(Table,n,1);
for i = 1:size(outputVals,2)
    Table(:,i) = table(outputVals(:,i));
end
% Assign column titles accordingly
Table.Properties.VariableNames = outputVars;
% Write table to comma-deliminated text file
filename = ['tableChoice',num2str(choice),'.txt'];
writetable(Table,filename);
