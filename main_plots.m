%%% Main file to generate secondary plots
clear;
clc;
close all;

%% Which parameter do you want to vary?
choice = 2;
% 1: plot from page 4
% 2: plot from page 20
% 3: plot from top of page 28
% 4: plot from bottom of page 28
% 5: plot from top of page 26
% 6: plot from bottom of page 26
% 7: plot from top page 27

%% 'Bad' conditions- based on typical ramjet engines
T_melt = 1700;  % Temperature at which the engine would melt [K]
h_ceil = 27432; % Engine suffocates [m] 

%% Define input variables
% Lecture 1 - ranges

n = 10000;        % Number of samples for sensitivity analysis [-]
% h             % Flying altitude [m]
h.min = 0;    
h.max = 45000;    
h.range = linspace(h.min, h.max, n);
h.avg = 20000;
[p_inf.avg, T_inf.avg] = atmosphere(h.avg);      % Calculate atmospheric properties for average height
[p_inf_bot, T_inf_bot] = atmosphere(0);
[p_inf_top, T_inf_top] = atmosphere(27432);

% M_flight      % Flying Mach number [-]
M_flight.min = 0;
M_flight.max = 10;
M_flight.range = linspace(M_flight.min, M_flight.max, n);
M_flight.avg = 2;
% S_shock       % Normal shock strength [-]
S_shock.min = 1.01;
S_shock.max = 2;
S_shock.range = linspace(S_shock.min, S_shock.max, n);
S_shock.avg = 1.2;
% M_burn        % Burner Mach number [-]
M_burn.min = 0.1;
M_burn.max = 0.3;
M_burn.range = linspace(M_burn.min, M_burn.max, n);
M_burn.avg = 0.2;
% T_burn        % Burner temperature [K]
T_burn.min = 10;
T_burn.max = 2200;
T_burn.range = linspace(T_burn.min, T_burn.max, n);
T_burn.avg = 1700;
% T_req         % Thrust required [N]
T_req.min = 50000;
T_req.max = 200000;
T_req.range = linspace(T_req.min, T_req.max, n);
T_req.avg = 100000;
% T_burnIn        % Burner inlet temperature [K]
T_burnerIn.min = 1000; %still need to change value
T_burnerIn.max = 2000;
T_burnerIn.range = linspace(T_burnerIn.min, T_burnerIn.max, n);
T_burnerIn.avg = 1500;

%% Define output variables for plotting
% calculateRamjet_plots.m contains more or less the same code as
% calculateRamjet.m, but with different variables as outputs to facilitate
% the creation of plots

% M_inlet       % Mach number at inlet (flight Mach) [-]
% M_burnerIn    % Mach number entering burner [-]
% M_burnerOut   % Mach number exiting burner [-]
% T_1           % Temperature at inlet (T_inf) [K]
% T_burnerIn    % Temperature entering burner [K]
% T_b           % Combuistion temperature [K]
% eta_th        % Thermodynamic efficiency [-]
% eta_prop      % Propulsive efficiency [-]
% A4_A1         % Area ratio of exhaust to inlet [-]

%set up matrix to store output values
outputVals = zeros(n,9);
        
%% Calculations

% Plot of burner entry mach number versus inlet mach number, for a constant
% temperature ratio of burner entry/inlet. Purpose is to show the mach
% number entering burner depends on ratio of temperatures, and that if
% inlet mach is too high, temperature inside combustion chamber will be too
% high.
if choice == 1 %slide page 4
    p_inf = [p_inf_bot, p_inf_top];
    T_inf = [T_inf_bot, T_inf_top];
    M_burnerIn_range = zeros(2,n);
    for j = 1:2     % 2 iterations for atmospheric conditions at h=0 and h=27432
        i = 0;          % Initialise counter
        for M_flight_c = M_flight.range
            i = i+1;    % Update counter

            [M_inlet,M_burnerIn,M_burnerOut,T_1,T_burnerIn,T_b...
            ,eta_th,eta_prop,A4_A1] = calculateRamjet_plots(p_inf(j),T_inf(j),M_flight_c,...
            S_shock.avg,(-j),T_burn.avg,T_req.avg);
        
            M_burnerIn_range(j,i) = M_burnerIn;
        end
    end
end

% Plot of combustion mach number (burner exit) as a function of temperature
% ratio (combustion/burner entry), for different constant values of burner
% entry mach number (M2). Purpose is to show the restrictions on burner
% mach numbers (entry and exit), as well as the restriction on combustion
% temperature. Gaps in plot indicate where a nonreal burner exit mach
% number arises from the calculations.
if choice == 2 %slide page 20
    M_2 = 0.2:0.1:0.5;              % burner entry mach number
    Mb = zeros(n,2,length(M_2));    % burner exit (combustion) mach number
    Tb_T2 = zeros(n,1);             % ratio of burner temperatures
    T_burn_c = linspace(10,2200,n); % variation of combustion temperatures
    for j = 1:length(M_2)
        for i = 1:length(T_burn_c)
            
            [M_inlet,M_burnerIn,M_burnerOut,T_1,T_burnerIn,T_b...
            ,eta_th,eta_prop,A4_A1] = calculateRamjet_plots(p_inf.avg,T_inf.avg,M_flight.avg,...
            S_shock.avg,M_2(j),T_burn_c(i),T_req.avg);

            Tb_T2(i) = T_burn_c(i)/T_burnerIn;
            
            % storing both positive and negative solutions to mach number
            % quadratic
            [Mb12] = Equations.burnerMach_AllSol(T_burnerIn,T_burn_c(i),M_2(j));
            Mb(i,1,j) = Mb12(1);
            Mb(i,2,j) = Mb12(2);
        end
    end
end

% Plot of propulsive efficiency as function of temperature ratio
% (combustion/inlet temperatures), for different inlet mach numbers
% Increasing burner temperature increases thrust, but decreases propulsive
% efficiency
if choice == 3 %slide page 28 top
    k = 0;
    Tb_T1 = zeros(1,n);             % temperature ratio
    Burnup = zeros(1,n*6);          % temperature at which engine melts
    eta_prop_range = zeros(6,n);    % propulsive efficiencies
    M_flight_c = 1:1:6;
    for j = 1:length(M_flight_c)          % inlet mach number
        i = 0;          % Initialise counter
        for T_burn_c = T_burn.range
            i = i+1;    % Update counter
            [M_inlet,M_burnerIn,M_burnerOut,T_1,T_burnerIn,T_b...
            ,eta_th,eta_prop,A4_A1] = calculateRamjet_plots(p_inf.avg,T_inf.avg,M_flight_c(j),...
            S_shock.avg,M_burn.avg,T_burn_c,T_req.avg);
            eta_prop_range(j,i) = eta_prop; 
            Tb_T1(i)=T_burn_c/T_1;
            
            T2=6.936*T_1;
            if T2/T_burn_c<1
                k=k+1;
                Burnup(k)=T_burn_c/T_1;             
            end
        end
    end
end


% Plot of thermodynamic, propulsive, and total efficiencies versus ratios
% of temperatures (burner entry/inlet)
if choice == 4 %slide page 28 bottom
    i=0;
    blackbird.h=24000; blackbird.M=3.2; blackbird.MC1=1.1;
    blackbird.M2=0.2; blackbird.Tb=1500; blackbird.F=100*1000;
    [p_inf_c, T_inf_c] = atmosphere(blackbird.h);
    
    for M_flight_c = linspace(0,6,100)
        i = i+1;
        [M_inlet,M_burnerIn,M_burnerOut,T_1,T_burnerIn,T_b...
                ,eta_th,eta_prop,A4_A1] = calculateRamjet_plots...
                (p_inf_c,T_inf_c,M_flight_c,blackbird.MC1,blackbird.M2,...
                blackbird.Tb,blackbird.F);
        
        % Write output values to outputVals matrix
        if M_burnerOut>0
            outputVals(i,:) = [M_inlet,M_burnerIn,M_burnerOut,T_1,T_burnerIn,T_b...
                ,eta_th,eta_prop,A4_A1];
        end
    end
    T2_T1 = outputVals(:,5)./outputVals(:,4);
    eta_th_range = outputVals(:,7);
    eta_prop_range = outputVals(:,8);
    eta_total = eta_th_range.*eta_prop_range;
    
    
end

if choice == 5 %slide page 28 top
    j = 0;
    k = 0;
    FPA_range = zeros(6,n);
    for M_flight_c = 1:6
        j = j+1;
        i = 0;          % Initialise counter
        for T_burn_c = T_burn.range
            i = i+1;    % Update counter
            [M_inlet,M_burnerIn,M_burnerOut,T_1,T_burnerIn,T_b...
            ,eta_th,eta_prop,A4_A1,F_PA] = calculateRamjet_plots...
            (p_inf.avg,T_inf.avg,M_flight_c,S_shock.avg,M_burn.avg,...
            T_burn_c,T_req.avg);
            T2=6.936*T_1;
            FPA_range(j,i) = F_PA; 
            Tb_T1(i)=T_burn_c/T_1;
            if T2/T_burn_c<1
                k=k+1;
                Burnup(k)=T_burn_c/T_1;             
            end
        end
    end
end

if choice == 6 %slide page 28 top
    j = 0;
    k=0;
    for M_burn_c = 0.1:0.1:0.5
        j = j+1;
        i = 0;          % Initialise counter
        for T_burn_c = T_burn.range
            i = i+1;    % Update counter
            [M_inlet,M_burnerIn,M_burnerOut,T_1,T_burnerIn,T_b...
            ,eta_th,eta_prop,A4_A1,F_PA] = calculateRamjet_plots...
            (p_inf.avg,T_inf.avg,M_flight.avg,S_shock.avg,M_burn_c,...
            T_burn_c,T_req.avg);
            T2=6.936*T_1;
            FPA_range(j,i)   = F_PA; 
            Tb_T1(i)=T_burn_c/T_1;
            if T2/T_burn_c<1
                k=k+1;
                Burnup(k)=T_burn_c/T_1;             
            end
        end
    end
end

if choice == 7 %slide page 28 top
    j = 0;
    k=0;
    for M_x_c = 1:2
        j = j+1;
        i = 0;          % Initialise counter
        for T_burn_c = T_burn.range
            i = i+1;    % Update counter
            [M_inlet,M_burnerIn,M_burnerOut,T_1,T_burnerIn,T_b...
            ,eta_th,eta_prop,A4_A1,F_PA] = calculateRamjet_plots...
            (p_inf.avg,T_inf.avg,M_flight.avg,M_x_c,M_burn.avg,...
            T_burn_c,T_req.avg);
            T2=6.936*T_1;
            FPA_range(j,i)   = F_PA; 
            Tb_T1(i)=T_burn_c/T_1;
            if T2/T_burn_c<1
                k=k+1;
                Burnup(k)=T_burn_c/T_1;             
            end
        end
    end
end

   
%% Plot generation

if choice == 1
    figure()
    M_burnerIn_range(imag(M_burnerIn_range) ~=0)=NaN;
    plot(M_flight.range, M_burnerIn_range)
    xlim([0, 10])
    xlabel('M_{flight} [-]')
    ylabel('M_{2} [-]')
    legend('T_{2}/T_{1} = 5.21','T_{2}/T_{1} = 6.69','Location','northwest')
end

if choice == 2
    figure()
    hold on
    for i = 1:length(M_2)
        plotLabel = sprintf('M = %2.1f',M_2(i));
        plot([Tb_T2;Tb_T2],[Mb(:,1,i);Mb(:,2,i)],'DisplayName',plotLabel);
    end
    hold off
    xlim([0,5.5])
    ylim([0,4])
    xlabel('T_{b}/T_{2} [-]')
    ylabel('M_{b} [-]') 
    legend('show')
end

if choice == 3
    figure()
    plot(Tb_T1, eta_prop_range)
    xlabel('T_{burn}/T_{1} [-]')
    ylabel('\eta_{prop} [-]')
    xlim([0 10])
    ylim([0.4 1])
    hold on
    plot(ones(1,3)*Burnup(1),0:2,'--')
    legend('M1 = 1','M1 = 2','M1 = 3','M1 = 4','M1 = 5','M1 = 6','Location','southwest');
end

if choice == 4
    figure()
    plot(T2_T1, eta_prop_range,T2_T1, eta_th_range, T2_T1, eta_total)
    legend('prop','thermo','total','Location','southeast')
    xlabel('T_{2}/T_{1} [-]')
    ylabel('\eta [-]') 
    xlim([0 8])
    ylim([0 1])
    legend('prop','thermo','total','Location','southwest');
end

if choice == 5
    figure()
    plot(Tb_T1, FPA_range)
    xlabel('$\frac{T_{burn}}{T_{1}} [-]$','Interpreter','latex')
    ylabel('$\frac{F}{P_{1}A_{1}} [-]$','Interpreter','latex')
    xlim([0 10])
    ylim([0 10])
    hold on
    plot(ones(1,11)*Burnup(1),0:10,'--')
    legend('M1 = 1','M1 = 2','M1 = 3','M1 = 4','M1 = 5','M1 = 6','Location','northwest');
end

if choice == 6
    figure()
    plot(Tb_T1, FPA_range)
    xlabel('$\frac{T_{burn}}{T_{1}} [-]$','Interpreter','latex')
    ylabel('$\frac{F}{P_{1}A_{1}} [-]$','Interpreter','latex')
    xlim([0 10])
    ylim([0 10])
    hold on
    plot(ones(1,11)*Burnup(1),0:10,'--')
    legend('M2 = 0.1','M2 = 0.2','M2 = 0.3','M2 = 0.4','M2 = 0.5','Location','northwest');
end

if choice == 7
    figure()
    plot(Tb_T1, FPA_range)
    xlabel('$\frac{T_{burn}}{T_{1}} [-]$','Interpreter','latex')
    ylabel('$\frac{F}{P_{1}A_{1}} [-]$','Interpreter','latex')
    xlim([0 10])
    ylim([0 10])
    hold on
    plot(ones(1,11)*Burnup(1),0:10,'--')
    legend('Mx = 1','Mx = 2','Location','northwest');
end

