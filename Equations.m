%% Equations class
% Equations.m is a class file that holds various equations needed to
% calculate flow properties for isentropic flow and normal shocks.

classdef Equations
    properties (Constant)
        gamma = 1.4; %Assumption made for air (ratio of specific heat)
    end
    methods (Static)
   
%% Isentropic flow relations
% input for functions is Mach number at previous stage
        % Temperature ratio
        function T0_T = tempRatio_Isen(M)
            gamma = Equations.gamma;
            T0_T = 1 + ((gamma-1)/2)*M^2;
        end
        
        % Pressure ratio
        function P0_P = pressRatio_Isen(M)
            gamma = Equations.gamma;
            P0_P = (1 + (gamma-1)/2*M^2)^(gamma/(gamma-1));
        end
        
        % Density ratio
        function rho0_rho = rhoRatio_Isen(M)
            gamma = Equations.gamma;
            rho0_rho = (1 + (gamma-1)/2*M^2)^(1/(gamma-1));
        end
        
        % Critical throat area
        function A_Astar = areaRatio_Isen(M)
            gamma = Equations.gamma;
            A_Astar = (1/M)*((2/(gamma+1))*(1+((gamma-1)/2)*M^2))^...
                ((gamma+1)/(2*(gamma-1)));
        end
%% Normal shock relations (non-isentropic)
% input for functions is Mach number immediately before normal shock
% (shock strength)
        % Mach number across shock
        function M2 = machNum_NShock(M1)
            gamma = Equations.gamma;
            M2 = sqrt(((gamma-1)*M1^2+2)/(2*gamma*M1^2-(gamma-1)));
        end

        % Density ratio
        function rho2_rho1 = rhoRatio_NShock(M)
            gamma = Equations.gamma;
            rho2_rho1 = ((gamma+1)*M^2) / ((gamma-1)*M^2 + 2);
        end
        
        % Pressure ratio
        function P2_P1 = pressRatio_NShock(M)
            gamma = Equations.gamma;
            P2_P1 = (2*gamma*M^2-(gamma-1)) / (gamma+1);
        end
        
        % Temperature ratio
        function T2_T1 = tempRatio_NShock(M)
            gamma = Equations.gamma;
            T2_T1 = ((2*gamma*M^2-(gamma-1))*(2+(gamma-1)*M^2))/...
                ((gamma+1)^2*M^2);
        end

%% Burner Mach number quadratic solver
        % Inputs: Temperature of flow entering burner (T2), Temperature of 
        %   flow required in burner (Tb), Mach number entering burner (M2)
        % Outputs: Mach number exiting burner
        function Mb = burnerMach(T2,Tb,M2)
            gamma = Equations.gamma;
            C1 = T2/Tb;
            C2 = (M2+1/(gamma*M2));
            if (C1*C2^2-4/gamma) < 0
                %Return NaN value for Mb if expression inside square root
                %is negative, resulting in nonreal roots/Mach numbers
                Mb = NaN;
                return
            end
            Mb1 = 0.5*sqrt(C1)*(C2) + 0.5*sqrt((C1)*(C2^2)-4/gamma);
            Mb2 = 0.5*sqrt(C1)*(C2) - 0.5*sqrt((C1)*(C2^2)-4/gamma);
            noChoke = [Mb1, Mb2];
            %Choose to return minimum burner exit mach number within the
            %non-choking conditions
            Mb = min(noChoke(noChoke<1 & noChoke>0));
        end
        
        % Return both solutions to quadratic equation, for plot generation
        % purposes only
        function Mb = burnerMach_AllSol(T2,Tb,M2)
            gamma = Equations.gamma;
            C1 = T2/Tb;
            C2 = (M2+1/(gamma*M2));
            if (C1*C2^2-4/gamma) < 0
                Mb = [NaN,NaN];
                return
            end
            Mb1 = 0.5*sqrt(C1)*(C2) + 0.5*sqrt((C1)*(C2^2)-4/gamma);
            Mb2 = 0.5*sqrt(C1)*(C2) - 0.5*sqrt((C1)*(C2^2)-4/gamma);
            Mb = [Mb1, Mb2];
        end
        
    end
end